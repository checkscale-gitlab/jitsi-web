#!/bin/sh

echo "Initializing Jitsi web…"

# enable Matomo support if requested
if [ "$MATOMO_ENABLE" = "true" ]; then
  sed -i -E 's/\/\/ (matomoEndpoint)/\1/' /templates/config.js
  sed -i -E 's/\/\/ (matomoSiteID)/\1/' /templates/config.js
fi

# customize configuration using environment variable
envsubst \
  < /templates/config.js \
  > /defaults/config.js

/init $@
