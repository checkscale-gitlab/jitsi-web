FROM jitsi/web:stable-5076

# update config.js
ENV CONFIG_RESOLUTION=720
ENV CONFIG_CONSTRAINTS_VIDEO_ASPECTRATIO="16 / 9"
ENV CONFIG_CONSTRAINTS_VIDEO_HEIGHT_IDEAL=720
ENV CONFIG_CONSTRAINTS_VIDEO_HEIGHT_MAX=720
ENV CONFIG_CONSTRAINTS_VIDEO_HEIGHT_MIN=240
ENV CONFIG_ANALYTICS_MATOMO_ENABLE=false
ENV CONFIG_ANALYTICS_MATOMO_ENDPOINT=https://your-matomo-endpoint/
ENV CONFIG_ANALYTICS_MATOMO_SITE_ID=0

# set some defaults values
ENV XMPP_DOMAIN=prosody
ENV XMPP_BOSH_URL_BASE=http://prosody:5280

RUN apt-get update && apt-get install -y wget gettext

RUN mkdir -p /templates

COPY config.js /templates/config.js
COPY watermark.png /usr/share/jitsi-meet/images/watermark.png

COPY init.sh /
RUN chmod +x /init.sh

ENTRYPOINT [ "/init.sh" ]
